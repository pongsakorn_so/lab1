package oop.lab1;

//TODO Write class Javadoc
/**
 * A student class that has name and id.It is inherited from Person
 * 
 * @author Your Name
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student and id is the id of the new Student.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object obj) {
		
		// (1) verify that obj is not null
		if (obj == null) return false;
		// (2) test if obj is the same class as "this" object
		if ( obj.getClass() != this.getClass() )
	 		return false;
		// (3) cast obj to this class's type
		Student other = (Student) obj;
		// (4) compare whatever values determine "equals"
		if ( id == other.id ) 
			return true;
		return false; 
	}
}
